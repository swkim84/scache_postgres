#ifdef CONFIG_SCACHE

#include <linux/falloc.h>
#include <sys/resource.h>

#define SCACHE_SET_CRITICAL -100
#define SCACHE_CLEAR_CRITICAL 100
#define SCACHE_SET_CRITICAL_INHERIT -99
#define SCACHE_CLEAR_CRITICAL_INHERIT 99

#endif
